<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210319071108 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE categorie_vente (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, description_categorie LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE exposition (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, numero_rue VARCHAR(255) NOT NULL, rue VARCHAR(255) NOT NULL, code_postal VARCHAR(5) NOT NULL, ville VARCHAR(255) NOT NULL, date DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE exposition_objet (exposition_id INT NOT NULL, objet_id INT NOT NULL, INDEX IDX_7DC0DCCD88ED476F (exposition_id), INDEX IDX_7DC0DCCDF520CF5A (objet_id), PRIMARY KEY(exposition_id, objet_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE lot (id INT AUTO_INCREMENT NOT NULL, vente_lot_id INT NOT NULL, nom VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, INDEX IDX_B81291BA7DF4DCE (vente_lot_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE objet (id INT AUTO_INCREMENT NOT NULL, lot_id INT NOT NULL, nom VARCHAR(100) NOT NULL, description_objet LONGTEXT NOT NULL, estimation INT NOT NULL, prix_dadjucation INT DEFAULT NULL, INDEX IDX_46CD4C38A8CBA5F7 (lot_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE personne (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, age SMALLINT NOT NULL, tel VARCHAR(255) NOT NULL, mail VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE personne_vente (personne_id INT NOT NULL, vente_id INT NOT NULL, INDEX IDX_9C6B3B25A21BD112 (personne_id), INDEX IDX_9C6B3B257DC7170A (vente_id), PRIMARY KEY(personne_id, vente_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE photo (id INT AUTO_INCREMENT NOT NULL, objet_id INT NOT NULL, chemin LONGTEXT NOT NULL, INDEX IDX_14B78418F520CF5A (objet_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE region (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(150) NOT NULL, description LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vente (id INT AUTO_INCREMENT NOT NULL, categorie_vente_id INT NOT NULL, region_vente_id INT NOT NULL, commissaire_priseur_id INT NOT NULL, nom VARCHAR(255) NOT NULL, date DATETIME NOT NULL, INDEX IDX_888A2A4C45405660 (categorie_vente_id), INDEX IDX_888A2A4CE0013F8 (region_vente_id), INDEX IDX_888A2A4CB4527B68 (commissaire_priseur_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE exposition_objet ADD CONSTRAINT FK_7DC0DCCD88ED476F FOREIGN KEY (exposition_id) REFERENCES exposition (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE exposition_objet ADD CONSTRAINT FK_7DC0DCCDF520CF5A FOREIGN KEY (objet_id) REFERENCES objet (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE lot ADD CONSTRAINT FK_B81291BA7DF4DCE FOREIGN KEY (vente_lot_id) REFERENCES vente (id)');
        $this->addSql('ALTER TABLE objet ADD CONSTRAINT FK_46CD4C38A8CBA5F7 FOREIGN KEY (lot_id) REFERENCES lot (id)');
        $this->addSql('ALTER TABLE personne_vente ADD CONSTRAINT FK_9C6B3B25A21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE personne_vente ADD CONSTRAINT FK_9C6B3B257DC7170A FOREIGN KEY (vente_id) REFERENCES vente (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE photo ADD CONSTRAINT FK_14B78418F520CF5A FOREIGN KEY (objet_id) REFERENCES objet (id)');
        $this->addSql('ALTER TABLE vente ADD CONSTRAINT FK_888A2A4C45405660 FOREIGN KEY (categorie_vente_id) REFERENCES categorie_vente (id)');
        $this->addSql('ALTER TABLE vente ADD CONSTRAINT FK_888A2A4CE0013F8 FOREIGN KEY (region_vente_id) REFERENCES region (id)');
        $this->addSql('ALTER TABLE vente ADD CONSTRAINT FK_888A2A4CB4527B68 FOREIGN KEY (commissaire_priseur_id) REFERENCES personne (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE vente DROP FOREIGN KEY FK_888A2A4C45405660');
        $this->addSql('ALTER TABLE exposition_objet DROP FOREIGN KEY FK_7DC0DCCD88ED476F');
        $this->addSql('ALTER TABLE objet DROP FOREIGN KEY FK_46CD4C38A8CBA5F7');
        $this->addSql('ALTER TABLE exposition_objet DROP FOREIGN KEY FK_7DC0DCCDF520CF5A');
        $this->addSql('ALTER TABLE photo DROP FOREIGN KEY FK_14B78418F520CF5A');
        $this->addSql('ALTER TABLE personne_vente DROP FOREIGN KEY FK_9C6B3B25A21BD112');
        $this->addSql('ALTER TABLE vente DROP FOREIGN KEY FK_888A2A4CB4527B68');
        $this->addSql('ALTER TABLE vente DROP FOREIGN KEY FK_888A2A4CE0013F8');
        $this->addSql('ALTER TABLE lot DROP FOREIGN KEY FK_B81291BA7DF4DCE');
        $this->addSql('ALTER TABLE personne_vente DROP FOREIGN KEY FK_9C6B3B257DC7170A');
        $this->addSql('DROP TABLE categorie_vente');
        $this->addSql('DROP TABLE exposition');
        $this->addSql('DROP TABLE exposition_objet');
        $this->addSql('DROP TABLE lot');
        $this->addSql('DROP TABLE objet');
        $this->addSql('DROP TABLE personne');
        $this->addSql('DROP TABLE personne_vente');
        $this->addSql('DROP TABLE photo');
        $this->addSql('DROP TABLE region');
        $this->addSql('DROP TABLE vente');
    }
}
