<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210324160338 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE exposition ADD vente_id INT NOT NULL');
        $this->addSql('ALTER TABLE exposition ADD CONSTRAINT FK_BC31FD137DC7170A FOREIGN KEY (vente_id) REFERENCES vente (id)');
        $this->addSql('CREATE INDEX IDX_BC31FD137DC7170A ON exposition (vente_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE exposition DROP FOREIGN KEY FK_BC31FD137DC7170A');
        $this->addSql('DROP INDEX IDX_BC31FD137DC7170A ON exposition');
        $this->addSql('ALTER TABLE exposition DROP vente_id');
    }
}
