<?php

namespace App\Entity;

use App\Repository\VenteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=VenteRepository::class)
 */
class Vente
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity=CategorieVente::class, inversedBy="listeVentes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $categorieVente;

    /**
     * @ORM\ManyToOne(targetEntity=Region::class, inversedBy="listeVentes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $regionVente;

    /**
     * @ORM\ManyToMany(targetEntity=Personne::class, mappedBy="ventes")
     */
    private $listePersonnes;

    /**
     * @ORM\ManyToOne(targetEntity=Personne::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $commissairePriseur;

    /**
     * @ORM\OneToMany(targetEntity=Lot::class, mappedBy="venteLot", orphanRemoval=true)
     */
    private $lotsVente;

    /**
     * @ORM\OneToMany(targetEntity=Exposition::class, mappedBy="vente", orphanRemoval=true)
     */
    private $expositions;

    public function __construct()
    {
        $this->listePersonnes = new ArrayCollection();
        $this->lotsVente = new ArrayCollection();
        $this->expositions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getCategorieVente(): ?CategorieVente
    {
        return $this->categorieVente;
    }

    public function setCategorieVente(?CategorieVente $categorieVente): self
    {
        $this->categorieVente = $categorieVente;

        return $this;
    }

    public function getRegionVente(): ?Region
    {
        return $this->regionVente;
    }

    public function setRegionVente(?Region $regionVente): self
    {
        $this->regionVente = $regionVente;

        return $this;
    }

    /**
     * @return Collection|Personne[]
     */
    public function getListePersonnes(): Collection
    {
        return $this->listePersonnes;
    }

    public function addListePersonne(Personne $listePersonne): self
    {
        if (!$this->listePersonnes->contains($listePersonne)) {
            $this->listePersonnes[] = $listePersonne;
            $listePersonne->addVente($this);
        }

        return $this;
    }

    public function removeListePersonne(Personne $listePersonne): self
    {
        if ($this->listePersonnes->removeElement($listePersonne)) {
            $listePersonne->removeVente($this);
        }

        return $this;
    }

    public function getCommissairePriseur(): ?Personne
    {
        return $this->commissairePriseur;
    }

    public function setCommissairePriseur(?Personne $commissairePriseur): self
    {
        $this->commissairePriseur = $commissairePriseur;

        return $this;
    }

    /**
     * @return Collection|Lot[]
     */
    public function getLotsVente(): Collection
    {
        return $this->lotsVente;
    }

    public function addLotsVente(Lot $lotsVente): self
    {
        if (!$this->lotsVente->contains($lotsVente)) {
            $this->lotsVente[] = $lotsVente;
            $lotsVente->setVenteLot($this);
        }

        return $this;
    }

    public function removeLotsVente(Lot $lotsVente): self
    {
        if ($this->lotsVente->removeElement($lotsVente)) {
            // set the owning side to null (unless already changed)
            if ($lotsVente->getVenteLot() === $this) {
                $lotsVente->setVenteLot(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Exposition[]
     */
    public function getExpositions(): Collection
    {
        return $this->expositions;
    }

    public function addExposition(Exposition $exposition): self
    {
        if (!$this->expositions->contains($exposition)) {
            $this->expositions[] = $exposition;
            $exposition->setVente($this);
        }

        return $this;
    }

    public function removeExposition(Exposition $exposition): self
    {
        if ($this->expositions->removeElement($exposition)) {
            // set the owning side to null (unless already changed)
            if ($exposition->getVente() === $this) {
                $exposition->setVente(null);
            }
        }

        return $this;
    }
}
