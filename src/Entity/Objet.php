<?php

namespace App\Entity;

use App\Repository\ObjetRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ObjetRepository::class)
 */
class Objet
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $nom;

    /**
     * @ORM\Column(type="text")
     */
    private $descriptionObjet;

    /**
     * @ORM\Column(type="integer")
     */
    private $estimation;

    /**
     * @ORM\ManyToMany(targetEntity=Exposition::class, mappedBy="objets")
     */
    private $expositions;

    /**
     * @ORM\ManyToOne(targetEntity=Lot::class, inversedBy="objets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $lot;
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $prix_dadjucation;

    /**
     * @ORM\OneToMany(targetEntity=Photo::class, mappedBy="objet", orphanRemoval=true)
     */
    private $photos;


    public function __construct()
    {
        $this->expositions = new ArrayCollection();
        $this->photos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescriptionObjet(): ?string
    {
        return $this->descriptionObjet;
    }

    public function setDescriptionObjet(string $descriptionObjet): self
    {
        $this->descriptionObjet = $descriptionObjet;

        return $this;
    }

    public function getEstimation(): ?int
    {
        return $this->estimation;
    }

    public function setEstimation(int $estimation): self
    {
        $this->estimation = $estimation;

        return $this;
    }

    /**
     * @return Collection|Exposition[]
     */
    public function getExpositions(): Collection
    {
        return $this->expositions;
    }

    public function addExposition(Exposition $exposition): self
    {
        if (!$this->expositions->contains($exposition)) {
            $this->expositions[] = $exposition;
            $exposition->addObjet($this);
        }

        return $this;
    }

    public function removeExposition(Exposition $exposition): self
    {
        if ($this->expositions->removeElement($exposition)) {
            $exposition->removeObjet($this);
        }

        return $this;
    }

    public function getLot(): ?Lot
    {
        return $this->lot;
    }

    public function setLot(?Lot $lot): self
    {
        $this->lot = $lot;

        return $this;
    }

    public function getPrixDadjucation(): ?int
    {
        return $this->prix_dadjucation;
    }

    public function setPrixDadjucation(?int $prix_dadjucation): self
    {
        $this->prix_dadjucation = $prix_dadjucation;

        return $this;
    }

    /**
     * @return Collection|Photo[]
     */
    public function getPhotos(): Collection
    {
        return $this->photos;
    }

    public function addPhoto(Photo $photo): self
    {
        if (!$this->photos->contains($photo)) {
            $this->photos[] = $photo;
            $photo->setObjet($this);
        }

        return $this;
    }

    public function removePhoto(Photo $photo): self
    {
        if ($this->photos->removeElement($photo)) {
            // set the owning side to null (unless already changed)
            if ($photo->getObjet() === $this) {
                $photo->setObjet(null);
            }
        }

        return $this;
    }
}
