<?php

namespace App\Entity;

use App\Repository\CategorieVenteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CategorieVenteRepository::class)
 */
class CategorieVente
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="text")
     */
    private $description_categorie;

    /**
     * @ORM\OneToMany(targetEntity=Vente::class, mappedBy="categorieVente", orphanRemoval=true)
     */
    private $listeVentes;

    public function __construct()
    {
        $this->listeVentes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescriptionCategorie(): ?string
    {
        return $this->description_categorie;
    }

    public function setDescriptionCategorie(string $description_categorie): self
    {
        $this->description_categorie = $description_categorie;

        return $this;
    }

    /**
     * @return Collection|Vente[]
     */
    public function getListeVentes(): Collection
    {
        return $this->listeVentes;
    }

    public function addListeVente(Vente $listeVente): self
    {
        if (!$this->listeVentes->contains($listeVente)) {
            $this->listeVentes[] = $listeVente;
            $listeVente->setCategorieVente($this);
        }

        return $this;
    }

    public function removeListeVente(Vente $listeVente): self
    {
        if ($this->listeVentes->removeElement($listeVente)) {
            // set the owning side to null (unless already changed)
            if ($listeVente->getCategorieVente() === $this) {
                $listeVente->setCategorieVente(null);
            }
        }

        return $this;
    }
}
