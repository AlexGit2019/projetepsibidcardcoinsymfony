<?php

namespace App\Entity;

use App\Repository\RegionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RegionRepository::class)
 */
class Region
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $nom;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=Vente::class, mappedBy="regionVente", orphanRemoval=true)
     */
    private $listeVentes;

    public function __construct()
    {
        $this->listeVentes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Vente[]
     */
    public function getListeVentes(): Collection
    {
        return $this->listeVentes;
    }

    public function addListeVente(Vente $listeVente): self
    {
        if (!$this->listeVentes->contains($listeVente)) {
            $this->listeVentes[] = $listeVente;
            $listeVente->setRegionVente($this);
        }

        return $this;
    }

    public function removeListeVente(Vente $listeVente): self
    {
        if ($this->listeVentes->removeElement($listeVente)) {
            // set the owning side to null (unless already changed)
            if ($listeVente->getRegionVente() === $this) {
                $listeVente->setRegionVente(null);
            }
        }

        return $this;
    }
}
