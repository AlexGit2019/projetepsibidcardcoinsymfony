<?php

namespace App\Entity;

use App\Repository\LotRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LotRepository::class)
 */
class Lot
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=Vente::class, inversedBy="lotsVente")
     * @ORM\JoinColumn(nullable=false)
     */
    private $venteLot;

    /**
     * @ORM\OneToMany(targetEntity=Objet::class, mappedBy="lot")
     */
    private $objets;

    public function __construct()
    {
        $this->objets = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getVenteLot(): ?Vente
    {
        return $this->venteLot;
    }

    public function setVenteLot(?Vente $venteLot): self
    {
        $this->venteLot = $venteLot;

        return $this;
    }

    /**
     * @return Collection|Objet[]
     */
    public function getObjets(): Collection
    {
        return $this->objets;
    }

    public function addObjet(Objet $objet): self
    {
        if (!$this->objets->contains($objet)) {
            $this->objets[] = $objet;
            $objet->setLot($this);
        }

        return $this;
    }

    public function removeObjet(Objet $objet): self
    {
        if ($this->objets->removeElement($objet)) {
            // set the owning side to null (unless already changed)
            if ($objet->getLot() === $this) {
                $objet->setLot(null);
            }
        }

        return $this;
    }

    public function getMoyenneEstimationObjets(): ?int
    {
         $objetsLot = $this->getObjets();
         $nombreObjets = count($objetsLot);
         if($nombreObjets != 0)
         {
             $sommeEstimations = 0;
             foreach($objetsLot as $objet)
             {
                 $sommeEstimations += $objet->getEstimation();
             }
             $moyenneEstimationsObjet = $sommeEstimations/$nombreObjets;
             return $moyenneEstimationsObjet;
         }
         else
         {
             return 0;
         }
    }
}
