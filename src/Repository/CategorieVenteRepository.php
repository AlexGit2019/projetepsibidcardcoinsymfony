<?php

namespace App\Repository;

use App\Entity\CategorieVente;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CategorieVente|null find($id, $lockMode = null, $lockVersion = null)
 * @method CategorieVente|null findOneBy(array $criteria, array $orderBy = null)
 * @method CategorieVente[]    findAll()
 * @method CategorieVente[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategorieVenteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CategorieVente::class);
    }

    // /**
    //  * @return CategorieVente[] Returns an array of CategorieVente objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CategorieVente
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
