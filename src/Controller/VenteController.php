<?php

namespace App\Controller;

use App\Entity\CategorieVente;
use App\Entity\Lot;
use App\Entity\Vente;
use App\Form\RechercheVenteType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class VenteController extends AbstractController
{
    /**
     * @Route("ventes/", name="ventes")
     */
    public function index(): Response
    {
        $repo = $this->getDoctrine()->getRepository(Vente::class);
        $ventes = $repo->findAll();
        return $this->render('vente/ventes.html.twig', [
            'ventes' => $ventes,
        ]);
    }

    /*public function venteLot($id_lot): Response
    {
        $repo = $this->getDoctrine()->getRepository(Lot::class);
        $lot = $repo->find($id_lot);
        $vente = $lot->getVenteLot();
        return $this->render('vente/_liste_ventes.html.twig', [
            'ventes' => [$vente],
        ]);
    }*/
    /**
     * @Route("/vente/{id_vente}", name="getVente")
     */
    public function getVente($id_vente): Response
    {
        $repo = $this->getDoctrine()->getRepository(Vente::class);
        $vente = $repo->find($id_vente);
        return $this->render('vente/getVente.html.twig', [
            'vente' => [$vente]
        ]);
    }
    /**
     * @Route("/vente/{id_vente}/lots", name="getLotsDuneVente")
     */
    public function getLotsDuneVente($id_vente)
    {
        $repo = $this->getDoctrine()->getRepository(Vente::class);
        $vente = $repo->find($id_vente);
        return $this->render('lot/lots_dune_vente.html.twig', ['vente' => $vente]);
    }
    /**
     * @Route("/ventes/rechercher", name="rechercherVentes")
     */
    public function rechercherVentes(Request $request)
    {
        // Création
        $form = $this->createForm(RechercheVenteType::class);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $formData = $form->getData();
            $repo = $this->getDoctrine()->getRepository(Vente::class);
            $ventes = $repo->findBy(['nom' => $formData->getNom(),'date'=> $formData->getDate(),'categorieVente' => $formData->getCategorieVente()]);
            return $this->render('vente/ventes_recherchees.html.twig', ['formData' => $formData, 'ventes' => $ventes]);
        }
        return $this->render("vente/ajouterVente.html.twig", ['form' => $form->createView()]);
    }

}
