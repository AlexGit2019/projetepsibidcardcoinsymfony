<?php

namespace App\Controller;

use App\Entity\CategorieVente;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategorieController extends AbstractController
{
    /**
     * @Route("/categorie", name="categorie")
     */
    public function index(): Response
    {
        return $this->render('categorie/index.html.twig', [
            'controller_name' => 'CategorieController',
        ]);
    }
    /**
     * @Route("/categorie/{id_categorie}", name="categorie")
     */
    public function categorie($id_categorie): Response
    {
        $repoCategorie = $this->getDoctrine()->getRepository(CategorieVente::class);
        $categorie = $repoCategorie->find($id_categorie);
        return $this->render('categorie/categorie.html.twig', [
            'categorie' => $categorie
        ]);
    }
}
