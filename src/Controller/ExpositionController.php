<?php

namespace App\Controller;

use App\Entity\Exposition;
use App\Entity\Objet;
use App\Entity\Vente;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ExpositionController extends AbstractController
{
    /**
     * @Route("/expositions", name="expositions")
     */
    public function expositions()
    {
        $repo = $this->getDoctrine()->getRepository(Exposition::class);
        $expositions = $repo->findAll();
        return $this->render('exposition/expositions.html.twig',['expositions' => $expositions]);
    }
    /**
     * @Route("/expositions/{id_objet}", name="expositionsObjet")
     */
    public function index($id_objet): Response
    {
        $repo = $this->getDoctrine()->getRepository(Objet::class);
        $objet = $repo->find($id_objet);
        return $this->render('exposition/expositions_objet.html.twig', [
            'objet'=> $objet,
        ]);
    }
    /**
     * @Route("/expositions/{id_vente}", name="expositionsVente")
     */
    public function expositionsVente($id_vente): Response
    {
        $repo = $this->getDoctrine()->getRepository(Vente::class);
        $vente = $repo->find($id_vente);
        return $this->render('exposition/expositions_vente.html.twig', [
            'vente'=> $vente,
        ]);
    }
}

