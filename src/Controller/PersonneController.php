<?php

namespace App\Controller;

use App\Entity\Personne;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PersonneController extends AbstractController
{
    /**
     * @Route("/personne", name="personne")
     */
    public function index(): Response
    {
        return $this->render('personne/index.html.twig', [
            'controller_name' => 'PersonneController',
        ]);
    }
    /**
     * @Route("/personnes/{idVente}", name="personnesVente")
     */
    /*public function personnesVentes($idVente) : Response
    {
        $repo = $this->getDoctrine()->getRepository(Personne::class);
        $vente = $repo->find($idVente);
        return $this->render('personne/personnes_vente.html.twig',[
            'vente' => $vente
        ]);
    }*/

}
