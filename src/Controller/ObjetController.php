<?php

namespace App\Controller;

use App\Entity\Exposition;
use App\Entity\Lot;
use App\Entity\Objet;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ObjetController extends AbstractController
{
    /**
     * @Route("/objets", name="objets")
     */
    public function index(): Response
    {
        //Récupération du repository qui va chercher les objets dans la BDD
        $repo = $this->getDoctrine()->getRepository(Objet::class);
        // Récupération des objets dans la BDD
        $objets = $repo->findAll();
        return $this->render('objet/objets.html.twig', [
            'objets' => $objets,
        ]);
    }
    /**
     * @Route("/objets/{idExpo}", name="objetsExposition")
     */
    public function objetsExposition($idExpo)
    {
        $repo = $this->getDoctrine()->getRepository(Exposition::class);
        $exposition = $repo->find($idExpo);
        $modele = ['exposition' => $exposition];
        return $this->render("exposition/objets_exposition.html.twig",$modele);
    }
    /**
     * @Route("/objets/{idLot}", name="objetsLot")
     */
    public function objetsLot($idLot)
    {
        $repo = $this->getDoctrine()->getRepository(Lot::class);
        $lot = $repo->find($idLot);
        $modele = ['lot' => $lot];
        return $this->render("objet/objets_lot.html.twig",$modele);
    }
}
