<?php

namespace App\Controller;

use App\Entity\Lot;
use App\Entity\Vente;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LotController extends AbstractController
{
    /**
     * @Route("/lot", name="lot")
     */
    public function index(): Response
    {
        return $this->render('vente/getVente.html.twig', [
        ]);
    }
}
