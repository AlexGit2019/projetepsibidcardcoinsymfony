Accès à l'application :

Ne pas utiliser l'URL https://alexandre.bidcardcoin.tk pour accéder à l'application, car, le site n'ayant pas de page d'accueil, il renvoie une page erreur 404. Utiliser l'URL https://alexandre.bidcardcoin.tk/ventes/ qui permet d'accéder à la liste des ventes.

Attention les liens  pour accéder aux expositions des ventes ne fonctionnent pas, car dans le contrôleur des expositions, deux routes ont la même signature d'URL, ce qui mène le système de route de Symfony en erreur. Pour régler le problème, il faudrait modifier l'URL d'une des routes dans le contrôleur.

Attention les liens  pour accéder aux objets d'un lot ne fonctionnent pas, car dans le contrôleur des expositions, deux routes ont la même signature d'URL, ce qui mène le système de route de Symfony en erreur. Pour régler le problème, il faudrait modifier l'URL d'une des routes dans le contrôleur.
